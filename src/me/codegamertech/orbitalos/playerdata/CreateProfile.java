package me.codegamertech.orbitalos.playerdata;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.codegamertech.orbitalos.computer.chat.ComputerChatMain;
import me.codegamertech.orbitalos.main.Main;

public class CreateProfile implements Listener {
	
	private static CreateProfile userProfile = new CreateProfile();
	public static CreateProfile getProfiles() {return userProfile;}
	
	private static Map<FileConfiguration, File> fileConfigurationFileMap = new HashMap<>();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		createUserFile(player.getUniqueId().toString(), player);
	}
	
	public FileConfiguration getUserFile(String uuid, Player player) {
		File tempFile = new File(Main.getInstance().getDataFolder() + "/players/", uuid + ".yml");
		if(!tempFile.exists()) {createUserFile(uuid, player);}
		FileConfiguration temp = YamlConfiguration.loadConfiguration(tempFile);
		save(tempFile, temp);
		fileConfigurationFileMap.put(temp, tempFile);
		return temp;
	}
	
	public void createUserFile(String uuid, Player player) {
		File tempFile = new File(Main.getInstance().getDataFolder() + "/players/", uuid + ".yml");
		if(!tempFile.exists()) {
			try {
				tempFile.createNewFile();
			} catch (IOException e) {
				Bukkit.getLogger().warning("A Internal Error Has Occured, Here is the Stack Trace:");
				e.printStackTrace();
			}
		}
		FileConfiguration temp = YamlConfiguration.loadConfiguration(tempFile);
		
		if(temp.contains("joins")) {
			temp.set("name", player.getName());
			temp.set("joins", temp.getInt("joins") + 1);
		} else {
			temp.set("name", player.getName());
			temp.set("joins", 1);
			temp.set("crypto.bal", 0);
		}
		
		fileConfigurationFileMap.put(temp, tempFile);
		save(tempFile, temp);

	}
	
	public void save(FileConfiguration fileConfiguration) {
		save(fileConfigurationFileMap.get(fileConfiguration), fileConfiguration);
	}
	
	public void save(File file, FileConfiguration fileConf) {
		try {
			fileConf.save(file);
		} catch (IOException e) {
			Bukkit.getLogger().warning("A Internal Error Has Occured, Here is the Stack Trace:");
			e.printStackTrace();
		}
	}
	
}

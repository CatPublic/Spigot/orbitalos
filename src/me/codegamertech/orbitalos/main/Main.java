package me.codegamertech.orbitalos.main;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.codegamertech.orbitalos.api.Loggr;
import me.codegamertech.orbitalos.computer.chat.ComputerChatMain;
import me.codegamertech.orbitalos.computer.chat.ComputerGUIChat;
import me.codegamertech.orbitalos.computer.gui.ComputerGUIMain;
import me.codegamertech.orbitalos.computer.listeners.OpenComputer;
import me.codegamertech.orbitalos.playerdata.CreateProfile;

public class Main extends JavaPlugin {
	
	public static Main inst;
	
	public void onEnable() {
		Loggr.SendLog("info", "Starting Loggr & Orbital OS");
		
		inst = this;
		PluginManager pm = getServer().getPluginManager();
		
		pm.registerEvents(new CreateProfile(), this);
		pm.registerEvents(new ComputerGUIMain(), this);
		pm.registerEvents(new OpenComputer(), this);
		pm.registerEvents(new ComputerChatMain(), this);
		pm.registerEvents(new ComputerGUIChat(), this);
	}
	
	public void onDisable() {
		Loggr.SendLog("info", "Stopping Loggr & Orbital OS");
	}

	public static JavaPlugin getInstance() {
		return inst;
	}
	
}

package me.codegamertech.orbitalos.computer.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.codegamertech.orbitalos.api.ItemMakr;
import me.codegamertech.orbitalos.computer.chat.ComputerGUIChat;
import me.codegamertech.orbitalos.icbms.Launch;

public class ComputerGUIMain implements Listener {
	
	private static Inventory inv;
	private static ItemStack notifications, finder, chat, crypto, market, icbm, terminal;
	
	public static Inventory Gui() {
		inv = Bukkit.getServer().createInventory(null, 45, ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &c Desktop"));
		
		notifications = ItemMakr.createItem(Material.BELL, "Notifications");
		
		finder = ItemMakr.createItem(Material.CHEST_MINECART, "Finder");
		chat = ItemMakr.createItem(Material.OAK_SIGN, "Chat");
		crypto = ItemMakr.createItem(Material.EMERALD, "Crypto Wallet");
		market = ItemMakr.createItem(Material.ITEM_FRAME, "Market");
		icbm = ItemMakr.createItem(Material.FIRE_CHARGE, "ICBM");
		terminal = ItemMakr.createItem(Material.DARK_OAK_SIGN, "Terminal");
		
		for(int i = 27; i <= 35; i++) {
			inv.setItem(i, ItemMakr.createItem(Material.BLACK_STAINED_GLASS_PANE, "Desktop"));
		}
		
		inv.setItem(36, notifications);
		
		inv.setItem(38, finder);
		inv.setItem(39, chat);
		inv.setItem(40, crypto);
		inv.setItem(41, market);
		inv.setItem(42, icbm);
		inv.setItem(43, terminal);
		
		return inv;
	}
	
	@EventHandler
	public void clickEvent(InventoryClickEvent event) {
		if(!event.getInventory().equals(inv)) return;
		if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR) return;
		if(event.getInventory().equals(inv)) event.setCancelled(true);
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Notifications")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rSorry! But this feature hasnt yet been implemented!"));
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Finder")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rSorry! But this feature hasnt yet been implemented!"));
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Chat")) {
			Inventory chatApp = ComputerGUIChat.Gui();
			event.getWhoClicked().closeInventory();
			event.getWhoClicked().openInventory(chatApp);
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Crypto Wallet")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rSorry! But this feature hasnt yet been implemented!"));
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Market")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rSorry! But this feature hasnt yet been implemented!"));
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("ICBM")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "LAUNCHING!"));
			Player player = (Player) event.getWhoClicked();
			Launch.launch(event.getWhoClicked().getLocation(), event.getWhoClicked().getLocation(), player);
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Terminal")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rSorry! But this feature hasnt yet been implemented!"));
			event.setCancelled(true);
		}
	}
	
}

package me.codegamertech.orbitalos.computer.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

import me.codegamertech.orbitalos.computer.gui.ComputerGUIMain;

public class OpenComputer implements Listener {
	
	@EventHandler
	public void playerInteractEvent(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Action action = event.getAction();
		Inventory inventory = ComputerGUIMain.Gui();
		
		if(action == Action.RIGHT_CLICK_BLOCK) {
			if(event.getMaterial() == Material.EMERALD_BLOCK && !player.isSneaking()) {
				player.openInventory(inventory);
				event.setCancelled(true);
			}
		}
		
	}
	
}

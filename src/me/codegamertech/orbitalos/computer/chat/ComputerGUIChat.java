package me.codegamertech.orbitalos.computer.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.codegamertech.orbitalos.api.ItemMakr;
import me.codegamertech.orbitalos.computer.gui.ComputerGUIMain;
import me.codegamertech.orbitalos.icbms.Launch;
import me.codegamertech.orbitalos.playerdata.CreateProfile;

public class ComputerGUIChat implements Listener {
	
	private static Inventory inv;
	private static ItemStack notifications, finder, chat, crypto, market, icbm, terminal, sendmessage, irc;
	
	public static Inventory Gui() {
		inv = Bukkit.getServer().createInventory(null, 45, ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &c Chat App"));
		
		notifications = ItemMakr.createItem(Material.BELL, "Notifications");
		
		finder = ItemMakr.createItem(Material.CHEST_MINECART, "Finder");
		chat = ItemMakr.createItem(Material.OAK_SIGN, "Chat");
		crypto = ItemMakr.createItem(Material.EMERALD, "Crypto Wallet");
		market = ItemMakr.createItem(Material.ITEM_FRAME, "Market");
		icbm = ItemMakr.createItem(Material.FIRE_CHARGE, "ICBM");
		terminal = ItemMakr.createItem(Material.DARK_OAK_SIGN, "Terminal");
		
		sendmessage = ItemMakr.createItem(Material.WRITABLE_BOOK, "Send Mail");
		irc = ItemMakr.createItem(Material.APPLE, "Apple IRC");
		
		inv.setItem(3, sendmessage);
		inv.setItem(5, irc);
		inv.setItem(7, ItemMakr.createItem(Material.BLACK_STAINED_GLASS_PANE, "gamer"));
		
		for(int i = 27; i <= 35; i++) {
			inv.setItem(i, ItemMakr.createItem(Material.BLACK_STAINED_GLASS_PANE, "Chat App"));
		}
		
		inv.setItem(36, notifications);
		
		inv.setItem(38, finder);
		inv.setItem(39, chat);
		inv.setItem(40, crypto);
		inv.setItem(41, market);
		inv.setItem(42, icbm);
		inv.setItem(43, terminal);
		
		return inv;
	}
	
	@EventHandler
	public void clickEvent(InventoryClickEvent event) {
		if(!event.getInventory().equals(inv)) return;
		if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR) return;
		if(event.getInventory().equals(inv)) event.setCancelled(true);
		
		//Application Buttons
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("gamer")) {
			FileConfiguration temp = CreateProfile.getProfiles().getUserFile(event.getWhoClicked().getUniqueId().toString(), (Player) event.getWhoClicked());
			
			event.getWhoClicked().sendMessage(temp.get("mail.sender").toString() + temp.get("mail.message").toString());
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Send Mail")) {
			if(ComputerChatMain.emailing.contains((Player) event.getWhoClicked())) {
				ComputerChatMain.emailing.remove((Player) event.getWhoClicked());
				event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cChatter &l- &rYou are no longer sending an email!"));
			} else {
				ComputerChatMain.emailing.add((Player) event.getWhoClicked());
				event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cChatter &l- &rYou are now sending an email, use the layout &b'<player name> <message>' &rto send the email!"));
				event.getWhoClicked().closeInventory();
			}
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Apple IRC")) {
			if(ComputerChatMain.chatting.contains((Player) event.getWhoClicked())) {
				ComputerChatMain.chatting.remove((Player) event.getWhoClicked());
				event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cChatter &l- &rYou are no longer talking in the Apple IRC board!"));
			} else {
				ComputerChatMain.chatting.add((Player) event.getWhoClicked());
				event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cChatter &l- &rYou are now talking in the Apple IRC board! Chat away... Type &b'/chat leave'&r or open the computer and reopen the chat app to leave the chat"));
				event.getWhoClicked().closeInventory();
			}
			event.setCancelled(true);
		}
		
		//General OS Buttons
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Notifications")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rSorry! But this feature hasnt yet been implemented!"));
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Finder")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rSorry! But this feature hasnt yet been implemented!"));
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Chat")) {
			Inventory desktopInv = ComputerGUIMain.Gui();
			event.getWhoClicked().closeInventory();
			event.getWhoClicked().openInventory(desktopInv);
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Crypto Wallet")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rSorry! But this feature hasnt yet been implemented!"));
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Market")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rSorry! But this feature hasnt yet been implemented!"));
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("ICBM")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "LAUNCHING!"));
			Player player = (Player) event.getWhoClicked();
			Launch.launch(event.getWhoClicked().getLocation(), event.getWhoClicked().getLocation(), player);
			event.setCancelled(true);
		}
		
		if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Terminal")) {
			event.getWhoClicked().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rSorry! But this feature hasnt yet been implemented!"));
			event.setCancelled(true);
		}
	}
	
}

package me.codegamertech.orbitalos.computer.chat;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import me.codegamertech.orbitalos.api.actionbarapi.ActionBar;
import me.codegamertech.orbitalos.playerdata.CreateProfile;

public class ComputerChatMain implements Listener {
	
	public static ArrayList<Player> chatting = new ArrayList<Player>();
	public static ArrayList<Player> emailing = new ArrayList<Player>();
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void sendMessageEvent(AsyncPlayerChatEvent event) {
		if(chatting.isEmpty() && emailing.isEmpty()) return;
		
		Player player = event.getPlayer();
		
		if(chatting.contains(player)) {
			event.setCancelled(true);
			for(Player recipients : chatting) {
				recipients.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cApple IRC > &r" + player.getName() + ": " + event.getMessage()));
				event.setCancelled(true);
			}
		}
		
		if(emailing.contains(player)) {
			event.setCancelled(true);
			
			String mailSplit[] = event.getMessage().split(" ", 2);
			
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cChatter - &rYou sent &b'" + mailSplit[0] + "'&r the message: &b'" + mailSplit[1] + "'&r."));
			
			OfflinePlayer recipient = Bukkit.getOfflinePlayer(mailSplit[0]);
			
			if(recipient.isOnline()) {
				Player recipientOnline = Bukkit.getPlayer(mailSplit[0]);
				String uuid = recipientOnline.getUniqueId().toString();
				FileConfiguration temp = CreateProfile.getProfiles().getUserFile(uuid, recipientOnline);
				
				ActionBar.sendActionBar(recipientOnline, "&cOrbitalOS &l- &rYou got a new notification for chatter!", 5000);
				recipientOnline.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cOrbitalOS &l- &rYou got a new notification for chatter!"));
				
				temp.set("mail.sender", player.getName());
				temp.set("mail.message", mailSplit[1]);
			} else {
				if(recipient.hasPlayedBefore()) {
					String uuid = recipient.getUniqueId().toString();
					FileConfiguration temp = CreateProfile.getProfiles().getUserFile(uuid, (Player) recipient);
					
					temp.set("mail.sender", player.getName());
					temp.set("mail.message", mailSplit[1]);
				} else {
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cChatter - &rMail could not send, please try again later. (Error Code #75b4a)"));
				}
			}
			
			emailing.remove(player);
		}
	}
	
}

package me.codegamertech.orbitalos.api;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemMakr {
	
	public static ItemStack createItem(Material material, String name) {
		ItemStack i = new ItemStack(material);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		i.setItemMeta(im);
		return i;
	}
	
	public static ItemStack createItem(Material material, String name, Enchantment enchantment) {
		ItemStack i = new ItemStack(material);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.addEnchant(enchantment, 0, false);
		i.setItemMeta(im);
		return i;
	}
	
	public static ItemStack createItem(Material material, String name, Enchantment enchantment, List<String> description) {
		ItemStack i = new ItemStack(material);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		im.addEnchant(enchantment, 0, false);
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	
}

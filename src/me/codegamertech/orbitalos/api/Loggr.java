package me.codegamertech.orbitalos.api;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Loggr {
	
	public static void SendLog(String type, String message) {
		if(type == "info") {
			System.out.println("Info: " + message);
			for(Player player : Bukkit.getOnlinePlayers()) {
				if(player.hasPermission("loggr.admin")) {
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b&lInfo: &r" + message));
				}
			}
		} else if(type == "warning") {
			System.out.println("Warning: " + message);
			for(Player player : Bukkit.getOnlinePlayers()) {
				if(player.hasPermission("loggr.admin")) {
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&lWarning: &r" + message));
				}
			}
		} else if(type == "critical") {
			System.out.println("Critical: " + message);
			for(Player player : Bukkit.getOnlinePlayers()) {
				if(player.hasPermission("loggr.admin")) {
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&lCritical: &r" + message));
				}
			}
		} else {
			System.out.println("Unknown: " + message);
			for(Player player : Bukkit.getOnlinePlayers()) {
				if(player.hasPermission("loggr.admin")) {
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b7&lUnknown: &r" + message));
				}
			}
		}
 	}
	
}
